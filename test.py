from sensornetwork.Communication.SensorUp import SensorUp
import json
import requests
"""
print "\nGET\n"

print "\nThings\n"
print SensorUp.getData("Things")

print "\nThings with id 1\n"
thingData = SensorUp.getData("Things", "1")
print thingData


print "\nSensor id\n"
sensorData = SensorUp.getData("Sensors", "4")
print sensorData


print "\nObserved Property ID\n"
OPData = SensorUp.getData("ObservedProperties", "5")
print OPData

print "\nObservation ID\n"
DSData = SensorUp.getData("Datastreams", "6")
print DSData

"""

print "\nPOST\n"

print "\nTHINGS\n"
"""
DODAVANJE THINGA
testData = {
  "name": "R1",
  "description": "Raspberry Pi number 1",
}
response = SensorUp.sendData(testData, "Things")
print response
print response["@iot.id"]
"""
print "\nLOCATIONS\n"

"""
DODAVANJE LOKACIJE S ID-em OD THINGA KOJEG SMO GETALI GORE
testData = {
  "name": "RPI1 Loc",
  "description": "Raspberry One Location",
  "encodingType": "application/vnd.geo+json",
  "location": {
    "type": "Point",
    "coordinates": [
      -114.130463,
      51.083693
    ]
  },
  "Things": [
    {
      "@iot.id": response["@iot.id"]
    }
  ]
}
print SensorUp.sendData(testData, "Locations")
"""

print "\nSENSORS\n"
"""
#DODAVANJE SENZORA
testData ={
  "name": "rpi",
  "description": "rpi test senzor",
  "encodingType": "text/html",
  "metadata": "http://www.lipsum.com/"
}

print SensorUp.sendData(testData, "Sensors")


print "\nOBSERVED PROPERTIES"
"""
"""
#DODAVANJE OBSERVED PROPERTIES-a
testData = {
  "name": "Movement",
  "definition": "https://en.wikipedia.org/wiki/Movement",
    "description": "Detecting movement"
}

print SensorUp.sendData(testData, "ObservedProperties")
"""
print "\nDATASTREAM\n"


#DODAVANJE DATASTREAMA
"""
testData={
  "name": "Movement data",
  "description": "Datastream for detecting movement",
  "observationType": "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
  "unitOfMeasurement": {
    "name": "None",
    "symbol": "none",
    "definition": "https://en.wikipedia.org/wiki/Humidity"
  },
  "Thing":{"@iot.id": 1},
  "ObservedProperty":{"@iot.id": 1602},
  "Sensor":{"@iot.id": 1594}
}
"""
print SensorUp.sendData(testData, "Datastreams")
"""
"""
"""
#BRISANJE
response = requests.delete("https://academic:8b59150a@academic-hr-ad60.sensorup.com/v1.0/Datastreams(43)")
"""
"""
print "\nOBSERVATION\n"
#DODAVANJE OBSERVAIONa
testData={
  "phenomenonTime": "2017-05-19T18:02:00.000Z",
  "resultTime" : "2017-05-19T18:02:05.000Z",
  "result" : 1,
  "Datastream":{"@iot.id": 778}

}

print SensorUp.sendData(testData, "Observations")
"""