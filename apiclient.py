from sensornetwork.Communication.ClientHttp import ClientHttp
from datetime import datetime

data = {
    "deviceId": 1,
    "sensorId": 2,
    "sensorType": "dht22",
    "timestamp": (datetime.utcnow() - datetime(1970, 1, 1)).total_seconds(),
    "data": 
    {
        "temperature": 14.12,
        "humidity": 0.43
    }
}

configurationData = {
    "deviceId": "1",
    "sensors": [
        {
            "sensorId": "2",
            "sensorType": "dht22",
            "sensorParameters": 
            {
                "pins": 
                {
                    "echoPin": 1,
                    "triggerPin": 3
                }
            }
        },
        {
            "sensorId": "3",
            "sensorType": "dht22",
            "sensorParameters": 
            {
                "pins": 
                {
                    "echoPin": 5,
                    "triggerPin": 7
                }
            }
        }
    ]
}

clientHttp = ClientHttp("http://127.0.0.1:8080/checkconfig"); #mozda bolje slati url kao parametar u sendData() ?
clientHttp.sendData(configurationData);