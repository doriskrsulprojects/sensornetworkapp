var sensorNetwork = angular.module('sensorNetwork', ['angularUtils.directives.dirPagination', 'ae-datetimepicker']);

sensorNetwork.controller ('getSensorData',
    function ($http, $scope, $timeout){
		
		var self = this;
		this.params  = {};
		this.sensorIds = [];
		this.sensorTypes = [];
		this.params.selectSensorId = "";
		this.params.selectedSensorType = "";
		$scope.sensorData = [];

		this.resetFilters = function() {
			$scope.selectedSensorType = "All sensor types";
			$scope.selectedSensorId = "All sensor IDs";
			this.dateFrom = moment().add(-5, 'd');
        	this.dateTo = moment();	
			self.setData("http://127.0.0.1:8080/sensordata");
		}
		
		this.setSensorIds = function(){
			self.sensorIds.push("All sensor IDs");
			$http.get("http://127.0.0.1:8080/sensortypes")
				.then(function(response) {
					this.returnData = response.data;
				})
				.finally(function(){
					angular.forEach(this.returnData, function(value, key) {
						this.push(key);
					}, self.sensorIds);
			});
		};
		
		this.setSensorTypes = function(){
			self.sensorTypes.push("All sensor types");
			$http.get("http://127.0.0.1:8080/sensordefinition")
				.then(function(response) {
					this.returnData = response.data;
				})
				.finally(function(){
					angular.forEach(this.returnData, function(value, key) {
						this.push(key);
					}, self.sensorTypes);
			});
		};
		
		this.setData = function(url) {
			$http.get(url)
				.then(function(response) {
					this.returnData = response.data;
				})
				.finally(function(){
					//if (this.returnData.length > 0)
					if(this.returnData)
						$scope.sensorData = this.returnData;
					else
						$scope.sensorData = [];
					dodajVarUJS($scope.sensorData);
			});
		};
        
		angular.element(document).ready(function() {
			$http.get("http://127.0.0.1:8080/sensordata")
			.then(function(response) {
				$scope.sensorData = response.data;
				self.numberPerPage = 3;
				self.currentPage = 1;
				self.numberOfItems = $scope.sensorData.length;
				self.maxPage = Math.ceil(self.numberOfItems/self.numberPerPage);
			});
			self.setSensorIds();
			self.setSensorTypes();
			$scope.selectedSensorType = "All sensor types";
			$scope.selectedSensorId = "All sensor IDs";
			dodajVarUJS($scope.sensorData);
			
		});

		this.sort = function(keyname){
			this.sortKey = keyname;
			this.reverse = !this.reverse;
		}

        this.dateFrom = moment().add(-5, 'd');
        this.dateTo = moment();

        this.optionsFrom = {format: 'DD.MM.YYYY'};
        this.optionsTo = {format: 'DD.MM.YYYY'};

        this.update = function (dateFrom, dateTo) {
            self.optionsFrom.maxDate = dateTo;
            self.optionsTo.minDate = dateFrom;
        };
        
        this.update(this.dateFrom, this.dateTo);
});

sensorNetwork.filter('filterBySensorId', function() {
    return function(sensors, sensorId) {
    	if (!angular.isDefined(sensorId) || sensorId == "All sensor IDs") return sensors;

        var result = [], i = 0;

        angular.forEach(sensors, function(inSensor) {
        	if(inSensor.sensorID === sensorId)
        	{
				result[i] = inSensor;
				i++;
			}

        });
        dodajVarUJS(result);
        return result;
    };
});

sensorNetwork.filter('filterBySensorType', function() {
    return function(sensors, sensorType, sensorId) {
    	if (!angular.isDefined(sensorType) || sensorType == "All sensor types" || sensorId != "All sensor IDs") 
    		return sensors;

        var result = [], i = 0;

        angular.forEach(sensors, function(inSensor) {
        	if(inSensor.sensorType === sensorType)
        	{
				result[i] = inSensor;
				i++;
			}

        });
        dodajVarUJS(result);
        return result;
    };
});

sensorNetwork.filter('filterByDate', function() {
    return function(sensors, from, to) {
        var result = [];
        var i = 0;

        var timeFrom = moment(from).format('X');
		var timeTo = moment(to).format('X');

        angular.forEach(sensors, function(inSensor) {
        	if(inSensor.timestamp >= timeFrom && inSensor.timestamp <= timeTo)
        	{
				result[i] = inSensor;
				i++;
			}

        });
        dodajVarUJS(result);
        return result;
    };
});
