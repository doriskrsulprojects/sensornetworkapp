from sensornetwork.Communication.ServerUdp import ServerUdp
from sensornetwork.Server.SensorServer import SensorServer
from sensornetwork.Server.CheckConfigurationServer import CheckConfigurationServer
from sensornetwork.Server.OnOffSensorServer import OnOffSensorServer
from sensornetwork.Server.ConfigurationServer import ConfigurationServer
from sensornetwork.Server.NotificationServer import NotificationServer
from sensornetwork.Server.NewComponentServer import NewComponentServer
from sensornetwork.Configuration.Configuration import Configuration
import SocketServer
import json
import sys
import thread
import threading
import time

def listenData(lock):
    #polje s uvjetima za slanje notifikacija, ako je uvjet iz polja zadovoljen primljeni podaci salju se na mail
    notificationConditions = Configuration.loadConfiguration('notificationConditions.json')

    #defaultna vrijednost host i port parametra ako se ne zadaju prilikom pokretanja skripte
    host, port = "localhost", 9998
    if len(sys.argv) == 3:
        host, port = sys.argv[1], int(sys.argv[2])
    elif len(sys.argv) == 2:
        host = sys.argv[1]
        port = port

    #stvaranje senor server objekta koji sluzi za obradu podataka i pokretanje UDP servera
    ServerUdp.sensorServer = SensorServer(notificationConditions)
    ServerUdp.lock = lock
    #ServerUdp.sensorServer = SensorServer(notificationConditions)
    #nakon stvaranja objekta sensorServer moze se pozvati funkcija koja omogucuje konekciju na bazu
    #korisnik bira hoce li se podaci slati na mysql bazu ili ne, ovisno o njegovom inputu
    #input = raw_input("Connect to database?Y/N ")
    #if (input == 'Y' or input == 'y'):
    #    dataServer.sensorServer.connectToDatabase()
    #pokretanje servera koji radi na zadanom hostu i portu i prima podatke s klijentske aplikacije
    server = SocketServer.UDPServer((host, port), ServerUdp)
    #ispis kako bi korisnik znao da je server ON nakon pokretanja ili ne pokretanja spajanja na bazu
    print "RPI data server is running"
    #server je upaljen dok ga prislino ne ugasimo ili dok ne dode do pogreske
    server.serve_forever()

def listenRpiNotification(lock):
    host, port = "localhost", 9997
    if len(sys.argv) == 3:
        host, port = sys.argv[1], int(sys.argv[2])
    elif len(sys.argv) == 2:
        host = sys.argv[1]
        port = port

    #napravit klasu za provjeru da li je doslo promjene u konfiguraciji i ukoliko je za slanje nove konfiguracije kao odg na rpi
    
    ServerUdp.configurationCheckServer = CheckConfigurationServer()
    ServerUdp.lock = lock
    server2 = SocketServer.UDPServer((host, port), ServerUdp)
    print "RPI configuration server is running"
    #server je upaljen dok ga prislino ne ugasimo ili dok ne dode do pogreske
    server2.serve_forever()


def listenAndriodNotification(lock):
    host, port = "localhost", 9996
    if len(sys.argv) == 3:
        host, port = sys.argv[1], int(sys.argv[2])
    elif len(sys.argv) == 2:
        host = sys.argv[1]
        port = port

    #napravit klasu za provjeru da li je doslo promjene u konfiguraciji i ukoliko je za slanje nove konfiguracije kao odg na rpi
    ServerUdp.onOffServer = OnOffSensorServer()
    ServerUdp.configurationServer = ConfigurationServer()
    ServerUdp.notificationServer = NotificationServer()
    ServerUdp.newComponentServer = NewComponentServer()
    ServerUdp.lock = lock
    server3 = SocketServer.UDPServer((host, port), ServerUdp)
    print "Android server is running"
    #server je upaljen dok ga prislino ne ugasimo ili dok ne dode do pogreske
    server3.serve_forever()    

try:
    
    lock = threading.Lock()
    
    t=threading.Thread(target=listenData, args=(lock,))
    t2=threading.Thread(target=listenRpiNotification, args=(lock,))
    t3=threading.Thread(target=listenAndriodNotification, args=(lock,))

    t.start()
    t2.start()
    t3.start()
   

except KeyboardInterrupt:
   print "Error: unable to start thread"
