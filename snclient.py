from sensornetwork.Sensors.SensorBase import SensorBase
from sensornetwork.Communication.ClientUdp import ClientUdp
from sensornetwork.Communication.DataSender import DataSender
from sensornetwork.Configuration.Configuration import Configuration
import socket
import time
import json
import sys
import thread
import threading
import signal

#import time
#from datetime import datetime

#defaultna vrijednost host i port parametra ako se ne zadaju prilikom pokretanja skripte
#klijentska aplikacija salje podatke na server koji slusa na tom host i portu

def sendData(lock, runEvent):
    host, port = "localhost", 9998
    if len(sys.argv) == 3:
        host, port = sys.argv[1], int(sys.argv[2])
    elif len(sys.argv) == 2:
        host = sys.argv[1]
        port = port
    #otvaranje socketa preko kojeg ce se razmjenivati podaci
    socket1 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    #data.json sadrizi podatke o svim senzorima prikacenim na rpi (sada je to harkodirano), ovaj dio treba preraditi
    #with open('data.json') as dataFile:    
    #    deviceConfiguration = json.load(dataFile)
    ####for sensorElement in deviceConfiguration["sensors"]:
        ##sensorElement["sensorClass"] = SensorBase.getSensor(sensorElement["sensorType"], sensorElement["sensorParameters"])
        #print sensorElement

    #pokretanje klijentske apikacija 
    #clientUdp = ClientUdp(host, port);
    sender = DataSender()
    sender.sendOverUdp(host, port)
    #slanje podataka na server dok se klijentska aplikacija ne zaustavi
    try:    
        while runEvent.is_set():
            deviceConfiguration = Configuration.loadConfiguration('dataRPI.json')

            #za svaki senzor za koji je instanca napravljena dohvati podatke i salji ih na server gdje se dalje obradjuju
            for sensorElement in deviceConfiguration["sensors"]:
                lock.acquire()
                sensorElement["sensorClass"] = SensorBase.getSensor(sensorElement["sensorType"], sensorElement["sensorParameters"])
                if(sensorElement["sensorParameters"]["sensorOn"]=="True"):
                    sensor = sensorElement["sensorClass"]
                    #za svaki senzor koji se nalazi u konfiguraciji dohvati podatke o njemu
                    data = sensor.getData()
                    for observation in data:
                        sendingData = {observation: data[observation]}
                        sender.sendData(sensorElement["sensorId"], sendingData, 0)
                lock.release()
                    #sensorData = {'data': data, 'deviceId': deviceConfiguration["deviceId"], 'sensorId': sensorElement["sensorId"], 'sensorType': sensorElement["sensorType"], 'timestamp': (datetime.utcnow() - datetime(1970, 1, 1)).total_seconds(), 'sensorParameters': sensorElement["sensorParameters"] }
                    #sensorData = sender.clientSender.sensorDataPacking(sensorElement["sensorId"], data)
                    #za svaki senzor iz konfiguracije posalji podatke na server
                    #sender.sendData(sensorElement["sensorId"], data)
                    
            time.sleep(5.5)
    #klijentsku aplikaciju mozemo prekinuti ctrl+c kombinacijom na tipkovnici
    except KeyboardInterrupt: 
        pass  

def askForConfiguration(lock, runEvent):
    host, port = "localhost", 9997
    if len(sys.argv) == 3:
        host, port = sys.argv[1], int(sys.argv[2])
    elif len(sys.argv) == 2:
        host = sys.argv[1]
        port = port
    #otvaranje socketa preko kojeg ce se razmjenivati podaci
    socket2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    deviceConfiguration = Configuration.loadConfiguration('dataRPI.json')

    sender2 = DataSender()
    sender2.sendOverUdp(host, port)

    try:    
        while runEvent.is_set():
            time.sleep(7)
            lock.acquire()
            #za svaki senzor za koji je instanca napravljena dohvati podatke i salji ih na server gdje se dalje obradjuju
            sender2.sendData(0, deviceConfiguration["deviceId"], 1)
            lock.release()
                    

    #klijentsku aplikaciju mozemo prekinuti ctrl+c kombinacijom na tipkovnici
    except KeyboardInterrupt: 
        pass  


try:
    runEvent = threading.Event()
    runEvent.set()    
    lock = threading.Lock()  

    t=threading.Thread(target=sendData, args=(lock, runEvent,))
    t2=threading.Thread(target=askForConfiguration, args=(lock,runEvent,))
    t.start()
    t2.start()

    while True: #na RPI-u umjesto while petlje ide signal.pause()
        time.sleep(1)# sluzi da bi mogli prekinut izvršavanje s ctrl+c

except (KeyboardInterrupt, SystemExit):
    print "closing"
    runEvent.clear()
    t.join()
    t2.join()
    print "closed"
